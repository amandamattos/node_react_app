if [ $SET_ROLLBACK = "true" ]
then
  export APP_VERSION=$ROLLBACK_VERSION
else
  export APP_VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')
fi

echo $$APP_VERSION 
sed -i 's/${ContainerTAG}/'"v$APP_VERSION"'/g' tw-teste-task-snapshot.json          
sed -i 's/${AWS_TASK_FAMILY}/'"$AWS_RELEASE_TASK_FAMILY"'/g' tw-teste-task-snapshot.json
sed -i 's/${IMAGE_REPO}/'"$IMAGE_REPO"'/g' tw-teste-task-snapshot.json 
sed -i 's/${CONTAINER_NAME}/'"$CONTAINER_NAME"'/g' tw-teste-task-snapshot.json 
aws ecs register-task-definition --cli-input-json file://tw-teste-task-snapshot.json
aws ecs update-service --cluster $AWS_ECS_CLUSTER --service $AWS_ECS_SERVICE --task-definition $AWS_RELEASE_TASK_FAMILY --force-new-deployment