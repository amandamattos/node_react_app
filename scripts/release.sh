if [ $SET_ROLLBACK = "false" ]
then
  git config --global user.email "$GIT_EMAIL"
  git config --global user.name "$GIT_USER_NAME"
  export APP_VERSION=$(npm version patch --allow-same-version -m "Updating release patch [skip ci]")
  echo "Nova versao da app:" "${APP_VERSION}"
  git push origin master --tags
  docker login -u $DOCKER_HUB_USERNAME --password $DOCKER_HUB_PASSWORD          
  docker build -t "${IMAGE_REPO}"/${CONTAINER_NAME}:"${APP_VERSION}" .
  docker tag "${IMAGE_REPO}"/${CONTAINER_NAME}:"${APP_VERSION}" "${IMAGE_REPO}"/${CONTAINER_NAME}:"${APP_VERSION}"
  docker push "${IMAGE_REPO}"/${CONTAINER_NAME}:"${APP_VERSION}"
  else 
    echo "Não gerou nova release - Rotina de Rollback em execução."
fi