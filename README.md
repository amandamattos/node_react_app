# Aplicação node e react - TW 
Simple node+react application to test pipeline development.
teste webhook

## Arquitetura

* Pipeline desenvolvido no Bitbucket Pipelines
* Pipeline as Code com .yml, e arquivos shell auxiliares (release.sh, deployTeste.sh, deploy.sh) e de propriedades (pipeProperties.sh)
* O Rollback ou escolha de versão é possibilitado através das propriedades SET_ROLLBACK=true e ROLLBACK_VERSION (setar no pipeProperties.sh) - O Bitbucket Pipelines não possui Build Parametrizado como o Jenkins
* Cluster AWS ECS - EC2, com possibilidade de AutoScaling de containers
* Deploys de Teste, Homologação e Produção são efetuados no mesmo Cluster
* Cada ambiente tem seu próprio ECS Service e seu próprio load balancer
* Credenciais AWS, DockerHub e Git foram cadastradas na interface do Bitbucket por razões de segurança
* Repositório para Infrastructure as Code: https://github.com/amandabatista/ECS_InfraAsCode
* O projeto de infra foi feito utilizando Terraform e também é necessário configurar as credenciais AWS
* O ambiente AWS é composto por: VPC, 2 Subnets Públicas (2 Available Zones), 1 Internet Gateway, 3 loag balancers, 3 Target Groups, ECS Cluster, 3 ECS Services, 3 Task Definition (e todas as dependências necessárias)