# base image
FROM node:6

# set working directory
RUN mkdir /usr/src/app
WORKDIR /usr/src/app

# add `/usr/src/app/node_modules/.bin` to $PATH
ENV PATH /usr/src/app/node_modules/.bin:$PATH

# install and cache app dependencies
# Bundle app source
COPY . .

RUN npm install

EXPOSE 3000

# start app
CMD ["npm", "start"]